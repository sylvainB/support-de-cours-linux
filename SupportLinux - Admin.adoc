= /ETRS/DGF/DSI/COURS SYSTEMES/LINUX: Administration CentOS 6
:doctype: book
:toc!:
:docinfo:
:encoding: utf-8
:lang: fr
:numbered:
:sectnumlevels: 2
:description: Support de cours Linux de l'Ecole des Transmissions de Rennes (ETRS)
:revnumber: 2.1
:revdate: 21-01-2017
:revremark: Ajout de cours 
:version-label!: 
:experimental:
:source-highlighter: coderay
:checkedbox: pass:normal[+&#10004;]+]

include::0000-preface.adoc[]

=== Gestion des versions

.Historique des versions du document
[width="100%",options="header",cols="1,2,4"]
|====================
| Version | Date | Observations 
| 1.0 | Février 2016 | Validation initiale
| 1.1 | 16 mars 2016 | Ajout du cours Samba
| 1.2 | 4 mai 2016 | Correction orthographique.
| 1.3 | 02 juin 2016 | Ajout du cours Postfix.
| 1.4 | 24 juin 2016 | Ajout du cours Apache.
| 1.5 | 13 juillet 2016 | Ajout des cours Système de fichiers et Gestion des tâches.
| 1.6 | 19 septembre 2016 | Ajout du cours scripts shell niveau 1.
| 2.0 | 21 novembre 2016 | Séparation des cours Admin et Services.
| 2.1 | 19 janvier 2017 | Ajout du cours gestion des utilitaires.
|====================

include::SYS-LNXBAS-010-presentation.adoc[leveloffset=+1]

include::SYS-LNXFON-020-commandes.adoc[leveloffset=+1]

include::SYS-LNXFON-030-utilisateurs.adoc[leveloffset=+1]

include::SYS-LNXFON-040-systeme-de-fichiers.adoc[leveloffset=+1]

include::SYS-LNXFON-050-gestion-processus.adoc[leveloffset=+1]

include::SYS-LNXFON-060-sauvegardes.adoc[leveloffset=+1]

include::SYS-LNXFON-070-demarrage.adoc[leveloffset=+1]

include::SYS-LNXFON-080-gestion-des-taches.adoc[leveloffset=+1]

include::SYS-LNXFON-090-mise-en-oeuvre-reseau.adoc[leveloffset=+1]

include::SYS-LNXUTI-020-gestion-des-utilitaires.adoc[leveloffset=+1]

include::SYS-LNXUTI-030-elevation-privilege-su-sudo.adoc[leveloffset=+1]

include::SYS-LNXSHL-010-Niveau1.adoc[leveloffset=+1]

include::SYS-LNXSHL-020-mecanismes-de-base.adoc[leveloffset=+1]

include::glossary.adoc[]

[index]
== Index
////////////////////////////////////////////////////////////////
The index is normally left completely empty, it's contents being
generated automatically by the DocBook toolchain.
////////////////////////////////////////////////////////////////  