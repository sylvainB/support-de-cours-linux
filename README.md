Support de cours Linux
======================

La dernière version du document est disponible dans le dossier build :

* [Version complète](https://framagit.org/alemorvan/support-de-cours-linux/blob/master/build/Support%20de%20cours%20Linux%20version%201.7.pdf)
* [Partie administration](https://framagit.org/alemorvan/support-de-cours-linux/blob/master/build/Support%20de%20cours%20Linux%20Admin%20version%202.1.pdf)
* [Partie service](https://framagit.org/alemorvan/support-de-cours-linux/blob/master/build/Support%20de%20cours%20Linux%20Services%20version%202.1.pdf)

Génération du document
----------------------

Le document PDF peut être généré avec le logiciel [AsciiDOCFX](http://asciidocfx.com/).

Licence
-------

Le document est sous licence Apache 2.0.


Problèmes connus
----------------

Les lignes de commandes de l'utilisateur root présentent un prompt à #. Ce caractère étant reconnu par Geshi comme un commentaire, tout ce qui suit est grisé.

Avant de générer le document, il conviendra de modifier la configuration de Geshi.

Remplacer la ligne du fichier [install_dir]/conf/docbook-conf/xslthl/bourne-hl.xml :

[source,xml]
----
<highlighter type="online-comment">#</highlighter>
----

par :

[source,xml]
----
<highlighter type="online-comment">##</highlighter>
----
